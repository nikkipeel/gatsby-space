### **SpaceX Data**

*This website fetches data from a SpaceX API to display information about the next scheduled launch, rockets, and the last 50 launches*
[SpaceX API](https://docs.spacexdata.com/)

#### Features
- Built with Gatsby using React hooks to fetch data and Tailwind for styling

#### Todos
- Add button with 'loadMore' functionality for pastLaunches component, limit 10 per click
- Consider upgrading pastLaunches and rockets to v4 (fetching the same data; prior issue: could not find all required params in upgraded API and was unable to properly sort by most recent launch)
- Add transition animations to elements
